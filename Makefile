.PHONY: all
all: check

.PHONY: check
check:
	sort -Vc README
	! grep -vE '^RFC [0-9]+ \S+(\s\S+)*$$' README

# vim:ts=4 sts=4 sw=4 noet
